# FramaWorld
A Workadventure Map in the style of Framasoft.
Framaworld is a fan-project. I am not an official member of Framasoft, I only support them in certain projects and Framaworld is a project to honor the work they do. However, Framasoft gave me permission to use their links. This is just a short note, that this project is not maintained by Framasoft.


# Contributing
👍🎉 First off, thanks for taking the time to contribute! 🎉👍

- You can contribute by helping out with tilesets for the map.
- Please upload to the "dev" branch. 

# Tilesets
- Octopus, Fox, NoGafam sign, Wizard hat by Anne Ghjklsky CC-BY 3.0
- "[LPC] Cats and Dogs" Artist: bluecarrot16 License: CC-BY 3.0 / GPL 3.0 / GPL 2.0 / OGA-BY 3.0 Please link to opengameart: http://opengameart.org/content/lpc-cats-and-dogs
- "[LPC] All Seasons Apple Tree: Tree trunk by Johann C. Shoot'em up graphic kit: http://opengameart.org/content/lpc-a-shootem-up-complete-graphic-kit CC-BY-SA 3.0
- "[LPC] Cats and Dogs" Artist: bluecarrot16 License: CC-BY 3.0 / GPL 3.0 / GPL 2.0 / OGA-BY 3.0 Please link to opengameart: http://opengameart.org/content/lpc-cats-and-dogs 
- C0c0bird (https://codeberg.org/c0c0bird | CC0 1.0 Universell (CC0 1.0)
- Open Game Art (https://opengameart.org | CC-BY-SA 3.0+ | see full credits here: https://framagit.org/kate/framaworld/-/blob/main/licenses/CREDITS-opengameart.txt
- Mullana | https://mullana.de/ | CC0 1.0 Universell (CC0 1.0)
- Carpet Texture - Red - Seamless Texture with normalmap | CC-BY 3.0 | Keith333 | https://opengameart.org/content/carpet-texture-red-seamless-texture-with-normalmap|           |
- Yolkati (Potion) | CC-BY-SA-3.0 & GPL 3.0 | GPL 2.0 |  https://opengameart.org/content/bottles 
- AnthonyMyers (Spellbook) CC-BY-3.0 | https://opengameart.org/content/spell-book
- Fantasy-Magic-Set by Melle | CC0 | https://opengameart.org/content/fantasy-magic-set
- Wooden Stairs NS | AntumDeluge| CC-BY 3.0 | CC-BY 3.0 | https://opengameart.org/content/wooden-stairs-ns
- Pepper and Carrot Picnic | crecs | CC-BY-4.0 | https://opengameart.org/content/pepper-and-carrot-picnic
- Vendors Table  | AntumDeluge | CC-BY-SA 3.0 | https://opengameart.org/content/vendors-table-lpc-table-rework
- Chaos Zone | https://github.com/chaosz0ne/shared | https://github.com/chaosz0ne/shared/blob/main/LICENSE
- Mountain Landscape |  Daniel Cook, Jetrel, Saphy (TMW), Zabin, Bertram | CC-BY 3.0 | https://opengameart.org/content/2d-lost-garden-zelda-style-tiles-resized-to-32x32-with-additions
- Tent | AntumDeluge | CC-BY 3.0 GPL 3.0 GPL 2.0 OGA-BY 3.0 | https://opengameart.org/content/tent-rework
- Mushrooms | AntumDeluge | CC-BY-SA 4.0 CC-BY-SA 3.0 | https://opengameart.org/content/mushrooms-remix
- Penguin | AntumDeluge | CC-BY-SA 4.0 | https://opengameart.org/content/penguin-rework
- Basic map 32x32 by Ivan voirol | CC-BY 3.0 | GPL 3.0 | GPL 2.0 | Ivan Voirol | https://opengameart.org/content/basic-map-32x32-by-ivan-voirol
- Alien| AntumDeluge | CC-BY-SA 3.0 | https://opengameart.org/content/alien-the-revolution
- Witch on Broomstick |AntumDeluge | CC-BY 4.0 CC-BY-3.0 OGA-BY 3.0 | https://opengameart.org/content/witch-on-broomstick
- Mushroom Houses | AntumDeluge | CC0 | https://opengameart.org/content/mushroom-houses
- David PictureFrame | Picture by David Revoy| Creative Commons Attribution 4.0 International license | https://www.peppercarrot.com/en/viewer/artworks-src__2020-04-19_mysterious_by-David-Revoy.html 
- [LPC] Farming tilesets, magic animations and UI elements | daneeklu |  Daniel Eddeland |  CC-BY-SA 3.0 | GPL 3.0 | https://opengameart.org/content/lpc-farming-tilesets-magic-animations-and-ui-elements

# Music 
- Ice Village | TAD | CC-BY-4.0 CCO | https://opengameart.org/content/iced-village-8-bit-lofi-hip-hop
- huh? | TinyWorlds | CC0 | https://opengameart.org/content/huh
- Fire Cracking | AntumDeluge | CC0 | https://opengameart.org/content/fire-crackling
- Witch Sound | AntumDeluge | CC0 | https://opengameart.org/content/witch-cackle


# Testing: In order to test this map on a workadenture server you need to have a CORS-Header add-on in your browser installed.